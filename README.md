##  Report Exporting Service

### Setup

Setup andactivate venv then:

```Pip install -r requirements.txt```

Enter config details into config.yaml:

```
host: <host>
db_name: <dnname>
username: <username>
password: <password>
```

### Use

To start server:

```python server.py```

To get PDF of report call:

```/report_exporting_service/api/v1.0/pdf?<report_id>```

To get XML of report call:

```/report_exporting_service/api/v1.0/xml?<report_id>```

### Tests

To run tests:

```python -m pytest```