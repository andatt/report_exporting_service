import pdfkit
import io
import psycopg2
import yaml
import json
from pathlib import Path
from flask import Flask, request, render_template, send_file, abort


app = Flask(__name__)

PATH_TO_CONFIG = Path("config.yaml").resolve()


class DBConn(object):

    def __init__(self, path_to_config):
        with open(path_to_config, "rb") as config_file:
            config = yaml.load(config_file, Loader=yaml.FullLoader)

        self.conn = psycopg2.connect(
            host=config["host"],
            user=config["username"],
            password=config["password"],
            database=config["db_name"]
        )
        self.cur = self.conn.cursor()


DB_CONN = DBConn(PATH_TO_CONFIG)


def report_template(report_id, template):
    """
    produces a rendered html template from given report id and template name
    :param report_id: int (assumption)
    :param template: string
    :return: rendered template
    """
    # done like this to avoid sql injection
    print("report_id", report_id)
    DB_CONN.cur.execute(
        "SELECT * FROM reports WHERE id = %(report_id)s",
        {"report_id": report_id}
    )
    raw_data_from_db = DB_CONN.cur.fetchone()  # fetchone as only expecting one report per id

    if not raw_data_from_db:
        # would make custom 404 page here if time
        return abort(404)

    data_from_db = json.loads(raw_data_from_db[1])

    return render_template(
        template,
        report_date=data_from_db["reported_at"],
        timestamp=data_from_db["created_at"],
        organisation=data_from_db["organization"],
        inventory=data_from_db["inventory"]

    )


@app.route('/report_exporting_service/api/v1.0/pdf', methods=['GET'])
def export_to_pdf():
    """
    Endpoint from getting a PDF version of the report_id specified

    :return: PDF file
    """
    report_id = request.args.get("report_id")
    template = "this_report_template.html"

    template_string = report_template(report_id, template)

    pdf_file = pdfkit.PDFKit(
        template_string,
        "string",
        css="static/styles/this_report_template.css"
    ).to_pdf()

    return send_file(io.BytesIO(pdf_file), mimetype="application/pdf")


@app.route('/report_exporting_service/api/v1.0/xml', methods=['GET'])
def export_to_xml():
    """
    Endpoint for getting an XML version of given report_id
    :return: XML
    """
    report_id = request.args.get("report_id")
    template = "this_report_template.html"

    # could make this download to xml file using send_to_file if required
    return report_template(report_id, template)


if __name__ == '__main__':
    app.run(debug=True)
