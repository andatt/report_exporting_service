import server
import pytest

# testing is incomplete not enough time


@pytest.mark.parametrize(
    ("url_params", "template", "expected"),
    (
        (1, "this_report_template.html", "Organisation: Dunder Mifflin"),
        (2, "this_report_template.html", "Organisation: MOM Corp."),
        (3, "this_report_template.html", "404"),  # dont really like this but short on time
        (4, "this_report_template.html", "Organisation: Flowers Inc")
    )
)
def test_xml_endpoint(url_params, template, expected):
    app = server.app.test_client()

    response = str(app.get("/report_exporting_service/api/v1.0/xml?report_id={}".format(url_params)).data)

    assert expected in response  # possibly a bit weak but short on time
